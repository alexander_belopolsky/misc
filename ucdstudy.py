"""
    >>> for c in range(maxcode):
    ...     char = chr(c)
    ...     if char.isspace():
    ...         print('U+%04X:' % c, ucd.name(char, '<no name>'))
    U+0009: <no name>
    U+000A: <no name>
    U+000B: <no name>
    U+000C: <no name>
    U+000D: <no name>
    U+001C: <no name>
    U+001D: <no name>
    U+001E: <no name>
    U+001F: <no name>
    U+0020: SPACE
    U+0085: <no name>
    U+00A0: NO-BREAK SPACE
    U+1680: OGHAM SPACE MARK
    U+180E: MONGOLIAN VOWEL SEPARATOR
    U+2000: EN QUAD
    U+2001: EM QUAD
    U+2002: EN SPACE
    U+2003: EM SPACE
    U+2004: THREE-PER-EM SPACE
    U+2005: FOUR-PER-EM SPACE
    U+2006: SIX-PER-EM SPACE
    U+2007: FIGURE SPACE
    U+2008: PUNCTUATION SPACE
    U+2009: THIN SPACE
    U+200A: HAIR SPACE
    U+2028: LINE SEPARATOR
    U+2029: PARAGRAPH SEPARATOR
    U+202F: NARROW NO-BREAK SPACE
    U+205F: MEDIUM MATHEMATICAL SPACE
    U+3000: IDEOGRAPHIC SPACE

"""

import unicodedata as ucd
maxcode = 1114111



def stripped(c):
    """
    >>> for c in range(maxcode):
    ...     char = chr(c)
    ...     if stripped(char):
    ...         print('U+%04X:' % c, ucd.name(char, '<no name>'))
    U+0009: <no name>
    U+000A: <no name>
    U+000B: <no name>
    U+000C: <no name>
    U+000D: <no name>
    U+001C: <no name>
    U+001D: <no name>
    U+001E: <no name>
    U+001F: <no name>
    U+0020: SPACE
    U+0085: <no name>
    U+00A0: NO-BREAK SPACE
    U+1680: OGHAM SPACE MARK
    U+180E: MONGOLIAN VOWEL SEPARATOR
    U+2000: EN QUAD
    U+2001: EM QUAD
    U+2002: EN SPACE
    U+2003: EM SPACE
    U+2004: THREE-PER-EM SPACE
    U+2005: FOUR-PER-EM SPACE
    U+2006: SIX-PER-EM SPACE
    U+2007: FIGURE SPACE
    U+2008: PUNCTUATION SPACE
    U+2009: THIN SPACE
    U+200A: HAIR SPACE
    U+2028: LINE SEPARATOR
    U+2029: PARAGRAPH SEPARATOR
    U+202F: NARROW NO-BREAK SPACE
    U+205F: MEDIUM MATHEMATICAL SPACE
    U+3000: IDEOGRAPHIC SPACE
    """
    return not c.strip()

def inttrail(c):
    """returns true if training c is accepted by int
    >>> for c in range(maxcode):
    ...     char = chr(c)
    ...     if inttrail(char):
    ...         print('U+%04X:' % c, ucd.name(char, '<no name>'))
    U+0009: <no name>
    U+000A: <no name>
    U+000B: <no name>
    U+000C: <no name>
    U+000D: <no name>
    U+0020: SPACE
    U+0085: <no name>
    U+00A0: NO-BREAK SPACE
    U+1680: OGHAM SPACE MARK
    U+180E: MONGOLIAN VOWEL SEPARATOR
    U+2000: EN QUAD
    U+2001: EM QUAD
    U+2002: EN SPACE
    U+2003: EM SPACE
    U+2004: THREE-PER-EM SPACE
    U+2005: FOUR-PER-EM SPACE
    U+2006: SIX-PER-EM SPACE
    U+2007: FIGURE SPACE
    U+2008: PUNCTUATION SPACE
    U+2009: THIN SPACE
    U+200A: HAIR SPACE
    U+2028: LINE SEPARATOR
    U+2029: PARAGRAPH SEPARATOR
    U+202F: NARROW NO-BREAK SPACE
    U+205F: MEDIUM MATHEMATICAL SPACE
    U+3000: IDEOGRAPHIC SPACE

    """
    try:
        ret = int('123' + c) == 123
    except ValueError:
        return False
    else:
        return ret