
M32 = 0xFFFFFFFF
M64 = 0xFFFFFFFFFFFFFFFF

def bcd(s):
    """bytes -> bcd

    >>> hex(bcd(b'12345'))
    '0x12345'
    """
    result = 0
    for (shift, b) in enumerate(reversed(s)):
        result |= (b & 0x0F) << 4 * shift
        h = hex(result), hex(b & 0x0F)
    return result


def bcdtoint32(x):
    """bcd int -> int
    >>> bcdtoint32(0x12345)
    12345
    """
    u = x
    M = [0xF0F0F0F0, 0xFF00FF00, 0xFFFF0000]
    N = [16 - 10, 16 ** 2 - 10 ** 2, 16 ** 4 - 10 ** 4]
    S = [4, 8, 16]

    for m, n, s in zip(M, N, S):
        u -= ((u & m) >> s) * n

    return u


def bcdtoint64(x):
    """bcd int -> int
    >>> bcdtoint64(0x12345)
    12345
    >>> bcdtoint64(0x1234567890123456)
    1234567890123456
    """
    u = x
    M = [0xF0F0F0F0F0F0F0F0, 0xFF00FF00FF00FF00, 0xFFFF0000FFFF0000, 0xFFFFFFFF00000000]
    N = [16 - 10, 16 ** 2 - 10 ** 2, 16 ** 4 - 10 ** 4, 16 ** 8 - 10 ** 8]
    S = [4, 8, 16, 32]

    for m, n, s in zip(M, N, S):
        u -= ((u & m) >> s) * n

    return u


def compress32(x, m):
    """           1   1   101         1010 0 0 0 0
        >>> m = 0b10001000111000000000111101010101
        >>> x = 0b10101010101010101010101010101010
        >>> bin(compress32(x, m))
        '0b1110110100000'
        >>> bin(compress32(m, m))
        '0b1111111111111'
        >>> hex(compress32(0x10203040, 0xF0F0F0F0))
        '0x1234'
        >>> hex(compress32(0x01020304, 0x0F0F0F0F))
        '0x1234'
    """
    x = x & m
    mk = (~m << 1) & M32

    for i in range(5):
        mp = mk ^ (mk << 1) & M32
        mp ^= (mp << 2) & M32
        mp ^= (mp << 4) & M32
        mp ^= (mp << 8) & M32
        mp ^= (mp << 16) & M32
        mv = mp & m
        m = m ^ mv | (mv >> (1 << i))
        t = x & mv
        x = x ^ t | (t >> (1 << i))
        mk &= ~mp
    return x


def compress64(x, m):
    """           1   1   101         1010 0 0 0 0
        >>> m = 0b10001000111000000000111101010101
        >>> x = 0b10101010101010101010101010101010
        >>> bin(compress64(x, m))
        '0b1110110100000'
        >>> bin(compress64(m, m))
        '0b1111111111111'
        >>> hex(compress64(0x10203040, 0xF0F0F0F0))
        '0x1234'
        >>> hex(compress64(0x01020304, 0x0F0F0F0F))
        '0x1234'
        >>> hex(compress64(0xA1B2C30405060708, 0x0F0F0F0F0F0F0F0F))
        '0x12345678'
    """
    x = x & m
    mk = (~m << 1) & M64

    for i in range(6):
        mp = mk ^ (mk << 1) & M64
        mp ^= (mp << 2) & M64
        mp ^= (mp << 4) & M64
        mp ^= (mp << 8) & M64
        mp ^= (mp << 16) & M64
        mp ^= (mp << 32) & M64
        mv = mp & m
        m = m ^ mv | (mv >> (1 << i))
        t = x & mv
        x = x ^ t | (t >> (1 << i))
        mk &= ~mp
    return x


def precompute32(m):
    """
    >>> m = int('0101 0101 0101 0101 0101 0101 0101 0101'.replace(' ', ''), 2)
    >>> for mv in precompute32(m):
    ...     print('{:032b}'.format(mv))
    01000100010001000100010001000100
    00110000001100000011000000110000
    00001111000000000000111100000000
    00000000111111110000000000000000
    00000000000000000000000000000000
    """
    mvs = [None] * 5
    mk = (~m << 1) & M32
    for i in range(5):
        mp = mk ^ (mk << 1) & M32
        mp ^= (mp << 2) & M32
        mp ^= (mp << 4) & M32
        mp ^= (mp << 8) & M32
        mp ^= (mp << 16) & M32
        mvs[i] = mv = mp & m
        m = m ^ mv | (mv >> (1 << i))
        mk &= ~mp

    return mvs


def precompute64(m):
    """
    >>> m = int('0101 0101 0101 0101 0101 0101 0101 0101'.replace(' ', ''), 2)
    >>> for mv in precompute64(m):
    ...     print('{:032b}'.format(mv))
    01000100010001000100010001000100
    00110000001100000011000000110000
    00001111000000000000111100000000
    00000000111111110000000000000000
    00000000000000000000000000000000
    00000000000000000000000000000000
    """
    mvs = [None] * 6
    mk = (~m << 1) & M64
    for i in range(6):
        mp = mk ^ (mk << 1) & M64
        mp ^= (mp << 2) & M64
        mp ^= (mp << 4) & M64
        mp ^= (mp << 8) & M64
        mp ^= (mp << 16) & M64
        mp ^= (mp << 32) & M64
        mvs[i] = mv = mp & m
        m = m ^ mv | (mv >> (1 << i))
        mk &= ~mp

    return mvs


def compress32p(x, m, mv=None):
    """
    >>> m = 0b10001000111000000000111101010101
    >>> x = 0b10101010101010101010101010101010
    >>> bin(compress32p(x, m))
    '0b1110110100000'
    >>> hex(compress32p(0x10203040, 0xF0F0F0F0))
    '0x1234'
    >>> m = 0x0F0F0F0F
    >>> mvs = precompute32(m)
    >>> [hex(x) for x in mvs]
    ['0x0', '0x0', '0xf000f00', '0xff0000', '0x0']
    >>> hex(compress32p(0x01020304, m, mvs))
    '0x1234'
    >>> hex(compress32p(0x09080706, m, mvs))
    '0x9876'
    """
    if mv is None:
        mv = precompute32(m)
    x = x & m
    t = x & mv[0];    x = x ^ t | (t >> 1)
    t = x & mv[1];    x = x ^ t | (t >> 2)
    t = x & mv[2];    x = x ^ t | (t >> 4)
    t = x & mv[3];    x = x ^ t | (t >> 8)
    t = x & mv[4];    x = x ^ t | (t >> 16)

    return x


def compress64p(x, m, mv=None):
    """
    >>> m = 0b10001000111000000000111101010101
    >>> x = 0b10101010101010101010101010101010
    >>> bin(compress64p(x, m))
    '0b1110110100000'
    >>> hex(compress64p(0x10203040, 0xF0F0F0F0))
    '0x1234'
    >>> m = 0x0F0F0F0F0F0F0F0F
    >>> mvs = precompute64(m)
    >>> [hex(x) for x in mvs]
    ['0x0', '0x0', '0xf000f000f000f00', '0xff000000ff0000', '0xffff00000000', '0x0']
    >>> hex(compress64p(0x01020304, m, mvs))
    '0x1234'
    >>> hex(compress64p(0x09080706, m, mvs))
    '0x9876'
    """
    if mv is None:
        mv = precompute64(m)
    x = x & m
    t = x & mv[0];    x = x ^ t | (t >> 1)
    t = x & mv[1];    x = x ^ t | (t >> 2)
    t = x & mv[2];    x = x ^ t | (t >> 4)
    t = x & mv[3];    x = x ^ t | (t >> 8)
    t = x & mv[4];    x = x ^ t | (t >> 16)
    t = x & mv[5];    x = x ^ t | (t >> 32)

    return x


def bcdpack32(x):
    """
    >>> hex(bcdpack32(0x01020304))
    '0x1234'
    """
    m = 0x0F0F0F0F
    mv2 = 0x0F000F00
    mv3 = 0x00FF0000
    x &= m
    t = x & mv2; x ^= t | (t >> 4)
    t = x & mv3; x ^= t | (t >> 8)

    return x


def bcdpack64(x):
    """
    >>> hex(bcdpack64(0x0102030405060708))
    '0x12345678'
    """
    m = 0x0F0F0F0F0F0F0F0F
    mv2 = 0xF000F000F000F00
    mv3 = 0x0FF000000FF0000
    mv4 = 0x000FFFF00000000
    x &= m
    t = x & mv2; x ^= t | (t >> 4)
    t = x & mv3; x ^= t | (t >> 8)
    t = x & mv4; x ^= t | (t >> 16)

    return x

def atoi64(s):
    """bytes -> int
    >>> atoi64(b'1234567890123456')
    1234567890123456
    >>> atoi64(b'123456789012')
    123456789012

    """
    lo = int.from_bytes(s[-16:-8], 'big')
    hi = int.from_bytes(s[-8:], 'big')

    x = bcdpack64(lo) << 32 | bcdpack64(hi)

    return bcdtoint64(x)