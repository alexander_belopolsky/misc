import unicodedata

# zone -> signs
simple_zones = {
    0x00C: ("+", "-\N{MINUS SIGN}"),  # ASCII
    0x066: ("", ""),  # ARABIC-INDIC
    0x06F: ("", ""),  # EXTENDED ARABIC-INDIC
    0x208: ("\N{SUBSCRIPT PLUS SIGN}", "\N{SUBSCRIPT MINUS}"),  # SUBSCRIPT
    0xFF1: ("\N{FULLWIDTH PLUS SIGN}", "\N{FULLWIDTH HYPHEN-MINUS}") # FULLWIDTH
}

superscripts = {
    '\N{SUPERSCRIPT ZERO}':  '0',
    '\N{SUPERSCRIPT ONE}':   '1',
    '\N{SUPERSCRIPT TWO}':   '2',
    '\N{SUPERSCRIPT THREE}': '3',
    '\N{SUPERSCRIPT FOUR}':  '4',
    '\N{SUPERSCRIPT FIVE}':  '5',
    '\N{SUPERSCRIPT SIX}':   '6',
    '\N{SUPERSCRIPT SEVEN}': '7',
    '\N{SUPERSCRIPT EIGHT}': '8',
    '\N{SUPERSCRIPT NINE}':  '9',
}

superscripts = {ord(c): superscripts[c] for c in superscripts}

def utoi(x):
    """
    >>> utoi('㊷')
    42
    >>> utoi('\N{ROMAN NUMERAL FIVE THOUSAND}')
    5000
    >>> utoi('⁻¹²³')
    -123
    >>> utoi('\N{LAO DIGIT ONE}\N{LAO DIGIT TWO}\N{LAO DIGIT THREE}')
    123
    """
    # Discard leading and trailing whitespace
    # U+0009: (HT)
    # U+000A: (LF)
    # U+000B: (VT)
    # U+000C: (FF)
    # U+000D: (CR)
    # U+001C: (FS)
    # U+001D: (GS)
    # U+001E: (RS)
    # U+001F: (US)
    # U+0020: SPACE
    # U+0085: NEXT LINE (NEL)
    # U+00A0: NO-BREAK SPACE
    # U+1680: OGHAM SPACE MARK
    # U+180E: MONGOLIAN VOWEL SEPARATOR
    # U+2000: EN QUAD
    # U+2001: EM QUAD
    # U+2002: EN SPACE
    # U+2003: EM SPACE
    # U+2004: THREE-PER-EM SPACE
    # U+2005: FOUR-PER-EM SPACE
    # U+2006: SIX-PER-EM SPACE
    # U+2007: FIGURE SPACE
    # U+2008: PUNCTUATION SPACE
    # U+2009: THIN SPACE
    # U+200A: HAIR SPACE
    # U+2028: LINE SEPARATOR
    # U+2029: PARAGRAPH SEPARATOR
    # U+202F: NARROW NO-BREAK SPACE
    # U+205F: MEDIUM MATHEMATICAL SPACE
    # U+3000: IDEOGRAPHIC SPACE
    u = x.strip()
    if len(u) == 1:
        # Accept any character with integer numeric value
        # Rationale: provide an obvious building block for parsers that scan strings by character.
        num = unicodedata.numeric(u)
        if int(num) == num:
            return int(num)

    # The last digit determines acceptable values
    last = u[-1]
    # Some scripts including ASCII have the property that the numerical of the digit
    # is found in the last 4 bits of the code point value.  The more common of these
    # scripts can be handled in C code without UCD lookup.
    zone = ord(last) >> 4
    if zone in simple_zones:
        if any(ord(c) >> 4 == zone for c in u):
            raise ValueError(x)
        pluses, minuses = simple_zones[zone]
        def value(u):
            return int(bytes(0x30|ord(c) & 0x0F for c in u))
    # Superscript variants are all over the code charts and require special handling.
    elif ord(last) in superscripts:
        u = u.translate(superscripts)
        pluses = '\N{SUPERSCRIPT PLUS SIGN}'
        minuses = '\N{SUPERSCRIPT MINUS}'
        def value(u):
            return int(u.encode('ascii'))
    elif unicodedata.category(last) == 'Nd':
        pluses = minuses = ""
        zero = ord(last) - int(unicodedata.numeric(last))
        def value(u):
            return sum((ord(d) - zero) * 10**n for n, d in enumerate(reversed(u)))

    first = u[0]
    if first in minuses:
        sign = -1
        u = u[1:]
    elif first in pluses:
        sign = 1
        u = u[1:]
    else:
        sign = 1

    return sign * value(u)